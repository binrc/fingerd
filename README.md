# usage

`fingerd(8)` is probably safer. If you even consider a finger daemon safe

# building 

```shell
sudo make install || doas make install
```

# inetd

Only tested against OpenBSD

```ini
finger		stream	tcp	nowait	_fingerd /opt/fingerd	/opt/fingerd
```

start inetd 

```shell
rcctl -f start inetd
```


# Systemd sockets

create `/etc/systemd/system/fingerd.socket`:

```ini
[Unit]
Description=fingerd

[Socket]
ListenStream=example.com:79
Accept=yes

[Install]
WantedBy=sockets.target
```

Create `/etc/system/system/fingerd@.service`:

```ini
[Unit]
Description=fingerd server
requires=fingerd.socket

[Service]
Type=simple
ExecStart=/opt/fingerd
StandardInput=socket
TimeoutStopSec=5

[Install]
WantedBy=multi-user.target
```

Good luck getting it working. 

# For users

fingerd gets it's information from dotfiles in your homedir. It reads from a few files: 

| File | Info associated|
|--------|--------|
| ~/.plan | usually nonsense | 
| ~/.project | whatever you're working on | 
| ~/.mailaddr | your email address (nonstandard) | 
| ~/.pgpkey | your pgp key (nonstandard) | 

### Example files

~/.plan

```text
-\b/\b|\b\\\b-\b/\b|\b\\\b-
```

~/.project

```text
workign on a finger server
```

~/.mailaddr

```text
foo@bar.baz
```

~/.pgpkey
```text
-----BEGIN PGP PUBLIC KEY BLOCK-----
Et1AjojJ8P+8ChC8W32v/6jLdrkXa5b0ra7dWbBuJuybbzA6/OwQJLnkrJOw1LdaUAEZncw6
oxE7LJW0wpLxRqfYvPoEZZtWoL6aFTh4nUHtDnZNarbiCSiru+3xJDHfTB9cK2uMyJiESUwH
/iH41i+DcuR2/6cb1VmuozUelouiSjkD3JieX+9w7skLMZ8bUqjM0CBu9cmTXH+XL1qm8NKP
XkZMKtxsyq2EOj7BAKMm3+j2cJU6ElyAi3Cek3ZI4jaznOhITXBUzMId9hW/RRcr0tooIYtl
ncofOnSZ2JfYgBUv6tjqEG/N2laERU03yzH+eH2uld3rYVTdnwicaPPkpMux4Uu07scL9/Uh
soqaB4fIkFGw1JDgG9pb3ZvbL7rWPkfy5gWKRl6YeO7JBkvyl7y1Q97o34vj95sTdruR7U0F
R827Q2BmG4e4ANqY8notozoxFTtC69Nrhh5iqJyhYZeWrmptiCugTSF3wxGwWRmhnXjJ6Z7n
IDoPtxzPubGj6DtQz78G5RzIrAlQt4JjOO+ZHfTlOcsimu5eyTlzaNzqZRpaB+WkrS5hIrB6
D0WM9+fT+2NZd13DfpLzdhgvng9tmmbGkx98H0sgJW2E9sFHPbO4vF8WPPXEtYzBG/2hsgNm
Y5+LMTMpKKfj+hiAOzCKma7B6Z69H1T2HEeWq/0gQmIfpsp2kE5Uh5+kTygYvD1UpLT48nla
7ikE5AMZqxoBzjE6vu8h/4q6L1Wc5zhRbVvs6EAdaAVEhJ+BdjsRgkwWLnqpteZ2hZgK==
-----END PGP PUBLIC KEY BLOCK-----
```
