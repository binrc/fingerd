#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>

int main(){
	char *username = (char *) malloc(32);		 // max UNIX username length
	char absolutepath[40] = "/home/";
	int n, i = 0;
	FILE *fp;
	char *fingerfiles[] = {".plan", ".project", ".mailaddr", ".pgpkey"};
	char tmpname[48];				// length of absolute path + longest file
	char c;
	char host[255]; 				// maximum domain name length

	fgets(username, sizeof(username), stdin);	// read from stdin
	username[strcspn(username, "\r\n")] = 0;	// strip newlines

	strcat(absolutepath,username);

	if(access(absolutepath, F_OK) != 0){
		// no enumerating users
		//printf("No such user %s\n", username);
		exit(1);
	}
		

	strcat(absolutepath, "/");

	// get hostname
	host[254] = '\0';
	gethostname(host, 254);
	printf("%s@%s\n\n", username, host);

	// loop through files
	for(i = 0; i < sizeof(fingerfiles)/sizeof(fingerfiles[0]); i++){
		memset(tmpname, 0, 48);
		strcat(tmpname, absolutepath);
		strcat(tmpname,	fingerfiles[i]);

		if((fp = fopen(tmpname, "r")) == NULL){
				printf("No %s\n", fingerfiles[i]);
		} else{
			while((c = fgetc(fp)) != EOF){
				printf("%c", c);
			}
			fclose(fp);
		}

		printf("\n");
	}

	free(username);
	return 0;
}
